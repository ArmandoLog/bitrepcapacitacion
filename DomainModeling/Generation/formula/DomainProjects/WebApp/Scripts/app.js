loadAdaptConfig(appData.resourcesUrl + 'Content/utils/adapt/');
Globalize.culture(appData.currentCulture);
/******************************************************************************/
$(document).ready(function() {
    $.tableSetup({
        bJQueryUI: true,
        aLengthMenu: [
            $.map(appData.itemsPerPage.split('|'), function(item) {
                return parseInt(item);
            }),
            $.map(appData.itemsPerPage.split('|'), function(item) {
                return parseInt(item);
            })],
        iDisplayLength: parseInt(appData.itemsPerPage.split('|')[0])
                //bLengthChange  : false
    });
    /**************************************************************************/
    $.comboboxSetup({
        delay: 900,
        textOverlabel: Globalize.localize('TextSimpleSearch'),
        messageLoading: $('<img />').addClass('ui-box-message-loading')
                .attr('title', Globalize.localize('TextLoading'))
                .attr('alt', Globalize.localize('TextLoading'))
                .attr('src', appData.resourcesUrl + 'images/loading-control.gif'),
        pageable: true,
        configButtonPrev: {
            label: 'Anterior',
            text: true,
            icons: {}
        },
        configButtonNext: {
            label: 'Siguiente',
            text: true,
            icons: {}
        }
    });
    /**************************************************************************/
    $('.box-fieldset .field-title').collapsibleContainer();
    /**************************************************************************/
    $.showPopupPageSetup({
        title: 'Gestión de Calendario de Clases'
    });
    /**************************************************************************/
    $.showCustomMessageSetup({
        title: 'Gestión de Calendario de Clases'
    });
    /**************************************************************************/
    $.showMessageSetup({
        title: 'Gestión de Calendario de Clases',
        minHeight: 80,
        timeOut: 1000
    });
    /**************************************************************************/
    //$.blockUI.defaults.message = 'Cargando...';
    $.blockUI.defaults.message = '<img src="' + appData.resourcesUrl + 'images/loading.gif' + '" />';
    $.blockUI.defaults.css.border = '0px';
    $.blockUI.defaults.css.backgroundColor = 'transparent';
    $.blockUI.defaults.overlayCSS.backgroundColor = '#000000';
    $.blockUI.defaults.overlayCSS.opacity = 0.5;
    $.blockUI.defaults.filter = 'Alpha(Opacity=50)';

    $.datepickerSetup({
        buttonImage: appData.resourcesUrl + "images/calendar.gif"
    });
    /**************************************************************************/
    $('.accordion').accordion();
    /**************************************************************************/
    $.ajaxSetup({
        cache: false,
        type: "POST",
        contentType: "application/json"
    });
    /**************************************************************************/
    $('.tabs').tabs();
    /**************************************************************************/
    $('.button').button();
    /**************************************************************************/
    $('input.datepicker').compDatepicker();
    /**************************************************************************/
    $('.box-current-user .btn-opciones').click(function() {
        $('.box-current-user .list-opciones').toggle('fade');
    });
});
/******************************************************************************/
appData.autoNumeric = {
    integerFormat: {
        mDec: 0,
        aSep: Globalize.culture().numberFormat[","],
        aDec: Globalize.culture().numberFormat["."]
    },
    decimalFormat: {
        mDec: 2,
        aSep: Globalize.culture().numberFormat[","],
        aDec: Globalize.culture().numberFormat["."]
    }
};

/******************************************************************************/
appData.redirectTo = function(route) {
    if ($.isPlainObject(route)) {
        $.ajax({
            url: appData.siteUrl + route.saveRoute,
            data: $.toJSON(route.saveParams),
            success: function() {
                appData.redirectTo(route.route);
            }
        });
    } else {
        if (route) {
            window.location.href = appData.siteUrl + route + "?navKey=" + appData.currentNavigationNodeKey;
        } else {
            window.location.href = appData.siteUrl + "?navKey=" + appData.currentNavigationNodeKey;
        }
    }
};
/******************************************************************************/
appData.goBack = function() {
    appData.redirectTo("GoBack");
};
/******************************************************************************/
/**
 * Muestra dialog o popup para un mensaje de success con warning
 */
function showMessageWithWarnings(title, message, warnings) {
    var dialog = $('<div />')
    $('<p />').text(message).appendTo(dialog);
    if ($.isArray(warnings) && warnings.length > 0) {
        var list = $('<ul class="list-errors" />');
        $.each(warnings, function(item, value) {
            $('<li class="item-warning" />').text(value).appendTo(list);
        });
        dialog.append(list);
    }
    dialog.dialog({
        title: title,
        modal: true,
        close: function(event, ui) {
            $('.list-errors', this).empty();
        }
    });
}

/**
 * generar html para parrafo resumido
 */
function summaryHtml(text, length, end) {

    return '<span title="' + text + '">'
        + trim(text.substr(0, length) + ((text.length > length) ? (isNull(end) ? '' : '...') : ''))
        + '</span>';
}

// *************************************************************************/
function setPersistenceStatus(selectRef, moduleKeyName) {
    if (!pageModulePrivileges[moduleKeyName].canCreate)
        selectRef.combobox('removeToolAdd');
    if (!pageModulePrivileges[moduleKeyName].canUpdate)
        selectRef.combobox('removeToolEdit');
}