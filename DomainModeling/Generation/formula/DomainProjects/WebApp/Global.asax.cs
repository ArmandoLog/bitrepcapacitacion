﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.WebPages;
using RazorGenerator.Mvc;
using Unir.Architecture.SuperTypes.PresentationBase;
using Unir.Architecture.SuperTypes.DistributedServicesBase;
using Unir.AppName.DistributedServices;

namespace Unir.AppName.WebDddApp
{
    public class WebApplication : MvcPresentationLayerBase<DddAppContainer>
    {
        protected override void Application_Start()
        {
            base.Application_Start();

            // Registrar la ruta por defecto
            RegisterDefaultRoute("Authentication", "Login");

            var engine = new PrecompiledMvcEngine(typeof(Unir.VisualComponents.ComponentsViews.Authorization.MenuView).Assembly);

            ViewEngines.Engines.Add(engine);
            VirtualPathFactoryManager.RegisterVirtualPathFactory(engine);

            ControllerBuilder.Current.DefaultNamespaces.Add("Unir.VisualComponents.Controllers");

            Bundles.Bundles.RegisterBundles(BundleTable.Bundles);
        }

        protected override void RegisterExtraReferences(IocContainer container)
        {
            container.AddCustomInjectionType(typeof(Unir.VisualComponents.Controllers.NotesController), "NotesService");
            container.AddCustomInjectionType(typeof(Unir.VisualComponents.Controllers.AuditingController), "AuditingService");

            base.RegisterExtraReferences(container);
        }
    }
}