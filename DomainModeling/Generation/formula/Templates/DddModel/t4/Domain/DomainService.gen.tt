﻿<#
string nameSpace = Properties.Find(p => p.WildCard == "Namespace").Value;

GetCsCodeGenFileComment();
WriteUsings(DomainService, nameSpace);
#>

namespace <#= nameSpace #>.DomainModules.<#= DomainService.Entity.Module #>.Services.Impl
{
    public partial class <#= DomainService.Name #>Base
    {
        protected readonly I<#= DomainService.Repository.Name #> Repo<#= DomainService.Entity.AggregateName #>;

        public <#= DomainService.Name #>Base(I<#= DomainService.Repository.Name #> repo<#= DomainService.Entity.AggregateName #>)
        {
            if (repo<#= DomainService.Entity.AggregateName #> == null) throw new ArgumentNullException("repo<#= DomainService.Entity.AggregateName #>");
				Repo<#= DomainService.Entity.AggregateName #> = repo<#= DomainService.Entity.AggregateName #>;
        }
		
<#
	foreach(var operation in DomainService.Operations)
	{
		WriteOperation(operation);
	}
#>	
    }
}
<#@ include file="DomainService.common.tt" #>
<#@ include file="..\GenHelper.tt" #>
<#+
void WriteOperation(Operation operation)
{
	var parameters = operation.Parameters.Select(p => p.Type + " " + p.Name)
										 .Aggregate((a, b) => a + ", " + b);
#>
		public virtual <#= operation.ReturnType #> <#= operation.Name #>(<#= parameters #>)
		{
<#+
			if(!"void".Equals(operation.ReturnType))
			{
#>			
			return <#= DefaultReturnValue(operation.ReturnType) #>;
<#+
			}
#>
		}
<#+
}

string DefaultReturnValue(string type)
{
	switch(type)
	{
		case "byte":
		case "int":
		case "long":
		case "decimal":
		case "double":
			return "0";
		case "DateTime":
			return "DateTime.Now";
		case "string":
		case "String":
			return "string.Empty";
		default:
			return "null";
	}
}
#>