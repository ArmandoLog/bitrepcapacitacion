<#
string nameSpace = Properties.Find(p => p.WildCard == "Namespace").Value;
string appName = Properties.Find(p => p.WildCard == "AppName").Value;

GetCsCodeGenFileComment();

SecurityOperationSettings = DddModelRoot.UnirDddModel.MvcModelRoot.SecurityOperationSettings;
#>

using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Unir.Architecture.SuperTypes.PresentationBase.Controllers;
using Unir.Architecture.SuperTypes.PresentationBase.ActionsParameters;
using Unir.Architecture.SuperTypes.PresentationBase.Attributes;
using Unir.Architecture.SuperTypes.PresentationBase.Attributes.ParametersPersistence;
using Unir.Architecture.SuperTypes.ApplicationServicesBase.Dto;
using Unir.Architecture.SuperTypes.ApplicationServicesBase.AppServicesUtility;
<#
WriteServicesUsings(Controller, nameSpace, appName);
#>

namespace <#= nameSpace #>.Web<#= appName #>.ApiControllers.<#= Controller.Entity.Module #>
{
	public class <#= Controller.Name #>Base : ApiControllerSuperType
	{
<#
	WriteConstructor(Controller);
	WriteReadingActions(Controller);
	WritePersistenceActions(Controller);
	WriteManyToManyActions(Controller);
	WriteFileManagementActions(Controller, null);
	WriteAggregateSubElementsActions(Controller);
#>
	}
}
<#@ include file="..\GenHelper.tt" #>
<#@ include file="ApiController.common.tt" #>
<#+
SecurityOperationSettings SecurityOperationSettings { get; set; }

void WriteConstructor(Controller controller)
{
	var entity = controller.Entity;
	var svc = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedServices[entity];
	
	var services = new List<ApplicationService> { svc };
#>
        protected readonly I<#= svc.Name #> Services<#= controller.Entity.AggregateName #>;
<#+
	if (entity.Parent != null && entity.InheritanceStrategy == InheritanceStrategy.Simple)
	{
		var svcParent = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedServices[entity.Parent];
		services.Add(svcParent);
#>
        protected readonly I<#= svcParent.Name #> Services<#= entity.Parent.AggregateName #>;
<#+
		if (entity.Parent.Parent != null && entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			var svcGrandpa = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedServices[entity.Parent.Parent];
			services.Add(svcGrandpa);
#>
        protected readonly I<#= svcGrandpa.Name #> Services<#= entity.Parent.Parent.AggregateName #>;
<#+
		}
	}
	
	var parameters = services.Select(s => "I" + s.Name + " services" + s.Entity.AggregateName).Aggregate((a, b) => a + ", " + b);
#>

        public <#= controller.Name #>Base(<#= parameters #>)
        {
<#+
	foreach(var pSvc in services)
	{
#>
            if (services<#= pSvc.Entity.AggregateName #> == null)
                throw new ArgumentNullException("services<#= pSvc.Entity.AggregateName #>");

            Services<#= pSvc.Entity.AggregateName #> = services<#= pSvc.Entity.AggregateName #>;
<#+
	}
#>
        }
<#+
}

void WriteScalarsFilterSearch(IEnumerable<ScalarProperty> scalars, string prefix = "")
{
	foreach(var scalar in scalars)
	{
		if(scalar.IsRangeType)
		{
#>
                Filter<#= prefix #><#= scalar.Name #> = new RangeFilterValues<<#= scalar.CSharpType #><#= scalar.CSharpType.EndsWith("?") ? "" : "?" #>>
                { 
                    From = parameters.Filter<#= prefix #><#= scalar.Name #>From,
                    To = parameters.Filter<#= prefix #><#= scalar.Name #>To
                },
<#+	
		}
		else if(scalar.IsFileType)
		{
#>
                FilterHas<#= prefix #><#= scalar.Name #> = parameters.FilterHas<#= prefix #><#= scalar.Name #>,
<#+	
		}
		else if("binary".Equals(scalar.Type))
		{
		}
		else
		{
#>
                Filter<#= prefix #><#= scalar.Name #> = parameters.Filter<#= prefix #><#= scalar.Name #>,
<#+	
		}
	}
}

string WriteAuthorizeAttribute(string securityModule, string securityOperation)
{
	return securityModule.StartsWith("roles:")
		? "[HttpRoleAuthorize(\"" + securityModule.Substring(6) + "\")]"
		: "[HttpModuleOperationAuthorize(\"" + securityModule + ": " + securityOperation + "\")]";
}

// ****************************************************************************** LECTURA **************************************************************************************************

void WriteReadingActions(Controller controller)
{
	var orderColumn = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedColumnOrders[controller.Entity];
	var specification = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedSpecifications[controller.Entity];
	var dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[controller.Entity];
#>

        #region Lectura
<#+
	if(controller.Actions.Any(a => a.Type == ControllerActionType.Get))
	{
		var allFiles = controller.Entity.PresentationHierarchicalScalars
									    .Where(s => s.IsFileType)
									    .ToList();
#>

        /// <summary>
        /// Devuelve el contenido de un objeto: <#= controller.Entity.Name #>
        /// </summary>
        /// <param name="id">Identificador de la entidad</param>
        /// <returns>El objeto <#= controller.Entity.Name #> o un error en caso de no existir</returns>
        [HttpGet]
		[Route("{id}", Name = "Get<#= controller.Entity.Name #>")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.ShowOperationName) #>
		public virtual IHttpActionResult Get(int id)
        {
			var result = Services<#= controller.Entity.AggregateName #>.Get<#= controller.Entity.Name #>(id);

            if (result.Type == ResultType.Ok && result.Element != null)
            {
<#+
		if (!allFiles.Any())
		{
#>
                return Ok(result.Element);
<#+
		}
		else
		{
			WriteGetFileTempIds(controller.Entity, allFiles);
		}
#>
            }
            return ResultWithMessages(result);
        }
<#+
	}
	
	var targetScalar = DddModelRoot.UnirDddModel.MvcModelRoot.GetSimpleSearchScalarFor(controller.Entity);	
	if(targetScalar != null)
	{
#>

        /// <summary>
        /// Devuelve una lista paginada y filtrada del objeto: controller.Entity.Name.
        /// </summary>
        /// <remarks>
        /// en el filtrado se usan los campos 'DisplayName'
        /// </remarks>
        /// <param name="searchText">Texto de filtrado</param>
        /// <param name="pageIndex">Índice de la página de los resultados</param>
        /// <param name="pageCount">Cantidad de elementos Devueltos</param>
        /// <returns>Los resultados</returns>
        [HttpGet]
        [Route("")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.ListOperationName) #>
		public virtual IHttpActionResult Get(string searchText = null, int? pageIndex = null, int? pageCount = null)
        {
			ResultListDto<SimpleListItemDto<int>> result;
		    
			if (pageIndex.HasValue)
            {
				result =
					Services<#= controller.Entity.AggregateName #>.GetPagedSimpleList<<#= dto.Name #>, int>(
						elem => elem.Id,
						elem => elem.<#= targetScalar.Name #>,
						searchText, pageIndex.Value, pageCount ?? PageCountConfiguredOptions[0]);		
			}
			else
			{
				result =
					Services<#= controller.Entity.AggregateName #>.GetSimpleList<<#= dto.Name #>, int>(
						elem => elem.Id,
						elem => elem.<#= targetScalar.Name #>,
						searchText);		
			}
			
            if (!result.HasErrors)
            {
                return OkPagedList(result);
            }
            return ResultWithMessages(result);
        }
<#+
	}	

	if(controller.Actions.Any(a => a.Type == ControllerActionType.Search))
	{
#>

        /// <summary>
        /// Devuelve los resultados de una "Búsqueda Avanzada" la cual consta de Filtrado, Proyección y Ordenacion Multiple
        /// </summary>
        /// <remarks>
        /// Almacena el objeto enviado como parámetro en el sistema de Estados de Presentación
        /// </remarks>
        /// <param name="parameters">Parámetros de la búsqueda</param>
        /// <returns>Los resultados.</returns>
        [HttpPost]
		[Route("advanced-search")]
        [HttpSaveParameters("<#= controller.Entity.AggregateName #>MainListParams")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.ListOperationName) #>
		public virtual IHttpActionResult AdvancedSearch(<#= controller.DefaultListParameter.Name #> parameters)
        {
            var pagination = parameters.GetPaginationInfo(<#= orderColumn.Name #>.<#= GetOrderingDefaultValue(controller.Entity) #>);

            var specification = new <#= specification.Name #>
            {
<#+
	WriteScalarsFilterSearch(controller.Entity.HierarchicalScalars);
	
	foreach(var nav in controller.Entity.HierarchicalNavigationProperties.Where(np => np.IsNavigable && !np.IsManyToMany && !np.IsPartOfAggregate))
	{
		if(nav.Entity is ValueObject)
		{
			WriteScalarsFilterSearch(nav.Entity.Scalars, nav.RoleName);
		}
		else
		{
#>
				FilterId<#= nav.RoleName #> = parameters.Filter<#= nav.RoleName #> != null ? parameters.Filter<#= nav.RoleName #>.Id : null,
<#+	
		}
	}
	
	foreach(var nav in controller.Entity.HierarchicalMultipleNavigationProperties.Where(np => np.IsNavigable && np.IsManyToMany))
	{
#>
                Filter<#= nav.RoleName #> = parameters.Filter<#= nav.RoleName #>,
<#+	
	}
#>
                Pagination = pagination
            };

            var result = Services<#= controller.Entity.AggregateName #>.GetPaged<#= Pluralize(controller.Entity.Name) #>(specification);

            if (!result.HasErrors)
            {
                return OkPagedList(result);
            }
            return ResultWithMessages(result);
        }
		
<#+
	}
#>

        #endregion
<#+
}
// ************************************************************************** PERSISTENCE **************************************************************************************************

void WritePersistenceActions(Controller controller)
{
	var dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[controller.Entity];
#>

        #region Persistencia
<#+	

	if(controller.Actions.Any(a => a.Type == ControllerActionType.Create))
	{
#>

        /// <summary>
        /// Añade un nuevo recurso/objeto: <#= controller.Entity.Name #>.
        /// </summary>
        /// <param name="parameters">Los valores de la nueva instancia a almacenar</param>
        /// <returns>La información del objeto creado. Error y Mensajes si dicha operación no se pudo completar</returns>
        [HttpPost]
        [Route("")]
        [HttpSaveResultData("Id", "DisplayName")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.SaveOperationName) #>
		public virtual IHttpActionResult New(<#= controller.DefaultSaveParameter.Name #> parameters)
        {
<#+
WriteSave(controller.Entity, controller.Entity, dto, "New" + controller.Entity.Name, false, null);
#>
        }
<#+
	}
	
	if(controller.Actions.Any(a => a.Type == ControllerActionType.Edit))
	{
#>

        /// <summary>
        /// Modifica un recurso/objeto: <#= controller.Entity.Name #> existente.
        /// </summary>
	    /// <param name="id">Identificador del elemento a Editar</param>		
        /// <param name="parameters">Los nuevos valores a establecer en la instancia existente</param>
        /// <returns>OK, si la operación se pudo completar con exito. NotFound o Conflict en caso de Error</returns>
        [HttpPut]
        [Route("{id}")]
        [HttpSaveResultData("Id", "DisplayName")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.SaveOperationName) #>
		public virtual IHttpActionResult Modify(int id, <#= controller.DefaultSaveParameter.Name #> parameters)
        {
<#+		
WriteSave(controller.Entity, controller.Entity, dto, "Modify" + controller.Entity.Name, true, "id");
#>
        }
<#+
	}
	
	if(controller.Actions.Any(a => a.Type == ControllerActionType.Delete))
	{
#>

        /// <summary>
        /// Elimina uno o varios recursos/objetos: <#= controller.Entity.Name #> existentes.
        /// </summary>
        /// <param name="parameters">Identificadores de los objetos que se desean eliminar del almacenamiento</param>
        /// <returns>OK, si la operación se pudo completar con exito. NotFound o Conflict en caso de Error</returns>
        [HttpPost]
		[Route("delete")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.DeleteOperationName) #>
        public virtual IHttpActionResult Delete(MultiSelectActionParameters parameters)
        {
            var result = Services<#= controller.Entity.AggregateName #>.Delete<#= Pluralize(controller.Entity.Name) #>(parameters.Ids.ToArray());

            if (result.Type == ResultType.Ok
				&& !result.HasErrors)
            {
                return Ok();
            }
            return ResultWithMessages(result);
        }
<#+
	}

	if(!controller.Entity.PresentationHierarchicalNavigationProperties
						 .Any(x => !x.IsMultiple && x.IsOneToOne && x.IsPrincipal) && controller.Entity.AutoGenerateIdentity)
	{
		if(controller.Actions.Any(a => a.Type == ControllerActionType.Clone))
		{
#>

        /// <summary>
        /// Intenta Clonar uno o varios recursos/objetos: <#= controller.Entity.Name #> existentes.
        /// </summary>
        /// <param name="parameters">Identificadores de los objetos que se desean clonar</param>
        /// <returns>OK, si la operación se pudo completar con exito. NotFound o Conflict en caso de Error</returns>
        [HttpPost]
		[Route("clone")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.CloneOperationName) #>
        public virtual IHttpActionResult Clone(MultiSelectActionParameters parameters)
        {
            var result = Services<#= controller.Entity.AggregateName #>.Clone<#= Pluralize(controller.Entity.Name) #>(parameters.Ids.ToArray());

            if (result.Type == ResultType.Ok
				&& !result.HasErrors)
            {
                return Ok();
            }
            return ResultWithMessages(result);
        }

<#+
		}
	}
#>
		#endregion
<#+
}

// ******************************************************************************** ACCIONES FILE-MANAGEMENT **********************************************************************************

void WriteGetFileTempIds(Entity entity, List<ScalarProperty> allFiles) 
{
#>
                // Procesar Ficheros
                var response = Request.CreateResponse(HttpStatusCode.OK, result.Element);				
<#+

	foreach(var fileScalar in allFiles)
	{
		var dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[entity];
		var scalarEntity = fileScalar.ContainerModelElement as Entity;
		var scalarEntityName = (fileScalar.ContainerModelElement as Entity).Name;

		var prefix = string.Empty;
		if (entity.Parent != null && entity.Parent.Scalars.Any(s => s.Name == fileScalar.Name) && entity.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			prefix = "." + entity.Parent.Name;			
		}
		else if (entity.Parent != null && entity.Parent.Parent != null && entity.Parent.Parent.Scalars.Any(s => s.Name == fileScalar.Name) && entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			prefix =  "." + entity.Parent.Name + "." + entity.Parent.Parent.Name;
		}
#>

                var resultFileId<#= fileScalar.Name #> = Services<#= entity.AggregateName #>.GetTempFileId<<#= dto.Name #>>(d => d<#= prefix #>.<#= fileScalar.Name #>);
				if (resultFileId<#= fileScalar.Name #>.Value != null)
					response.Headers.Add("X-TempFileId-<#= fileScalar.Name #>", resultFileId<#= fileScalar.Name #>.Value.ToString());
<#+	
	}
	
#>

				return ResponseMessage(response);	
<#+
}

void WriteFileManagementActions(Controller controller, NavigationProperty aggregatePart)
{
	var allInheritanceFiles = controller.Entity.PresentationHierarchicalScalars
										       .Where(s => s.IsFileType)
											   .ToList();
	List<ScalarProperty> allAggregateFiles = null;
	
	if(aggregatePart != null && controller.Entity is Aggregate)
	{
		var aggregate = controller.Entity as Aggregate;
		allAggregateFiles = aggregatePart.Entity.PresentationHierarchicalScalars.Where(s => s.IsFileType)
												.ToList();
	}
	else
	{
		allAggregateFiles = new List<ScalarProperty>();
	}
	
	IList<ScalarProperty> allFiles = null;

	if(aggregatePart != null)
	{
		allFiles = allAggregateFiles;
	}
	else
	{
		allFiles = allInheritanceFiles;
	}						 

	if(!allFiles.Any())
		return;
#>

        #region Gestión de Ficheros
<#+
	foreach(var scalar in allFiles)
	{
		// var isControllerEntityScalar --> Related to Files in aggregate parts fix: No support for hidding actions from UI model for aggregate parts.
		
		var dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[controller.Entity];
		var scalarEntity = scalar.ContainerModelElement as Entity;
		var scalarEntityName = (scalar.ContainerModelElement as Entity).Name;
		var isControllerEntityScalar = (scalarEntityName == controller.Entity.Name);
		var svcEntity = controller.Entity;
		
		if (controller.Entity.Parent != null && controller.Entity.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			if(controller.Entity.Parent.HierarchicalScalars.Any(s => s.Name.Equals(scalar.Name)))
			{
				dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[controller.Entity.Parent];
				svcEntity = controller.Entity.Parent;
			}
			
			if (controller.Entity.Parent.Parent != null && controller.Entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
			{
				if(controller.Entity.Parent.Parent.HierarchicalScalars.Any(s => s.Name.Equals(scalar.Name)))
				{
					dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[controller.Entity.Parent.Parent];
					svcEntity = controller.Entity.Parent.Parent;
				}
			}
		}
		
		if(!isControllerEntityScalar)
		{
			dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[scalarEntity];
		}
		
		if(!isControllerEntityScalar || controller.Actions.Any(a => a.Type == ControllerActionType.File && a.PropertyName != null && a.PropertyName.Equals(scalar.Name)))
		{
			var modelName = isControllerEntityScalar ? scalar.Name : scalarEntityName + scalar.Name;
#>

<#+
			if("image".Equals(scalar.Type))
			{
#>

        /// <summary>
        /// Descarga el contenido de una imagen asociada a la propiedad: <#= modelName #> de una entidad: <#= svcEntity.Name #>
        /// </summary>
        /// <param name="id<#= svcEntity.Name #>">Identificador del objeto.</param>
        /// <returns>El contenido dela imagen en Base64.</returns>
        [HttpGet]
		[Route("{id<#= svcEntity.Name #>}/<#= Hyphenate(modelName) #>-thumb")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.ShowOperationName) #>
		public virtual IHttpActionResult GetThumb<#= modelName #>(int id<#= svcEntity.Name #>)
        {
            var result = Services<#= svcEntity.AggregateName #>.<#= scalarEntityName #>Download<#= scalar.Name #>(id<#= svcEntity.Name #>);

			if (result.HasErrors)
            {
                return ResultWithMessages(result);
            }		
            return ImageThumb(result.Content);	
        }
<#+	
			}
			else
			{
#>

        /// <summary>
        /// Descarga el archivo asociado a la propiedad: <#= modelName #> de una entidad: <#= svcEntity.Name #>
        /// </summary>
        /// <param name="id<#= svcEntity.Name #>">Identificador del objeto.</param>
        /// <returns>El contenido del fichero.</returns>
        [HttpGet]
		[Route("{id<#= svcEntity.Name #>}/download-<#= Hyphenate(modelName) #>")]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.DownloadFileOperationName) #>
        public virtual IHttpActionResult Download<#= modelName #>(int id<#= svcEntity.Name #>)
        {
            var result = Services<#= svcEntity.AggregateName #>.<#= scalarEntityName #>Download<#= scalar.Name #>(id<#= svcEntity.Name #>);

			if (result.HasErrors)
            {
                return ResultWithMessages(result);
            }		
            return File(result.FileName, result.Content);	
        }
<#+	
			}
		}
	}
#>

        #endregion Gestión de Ficheros
<#+
}

// ******************************************************************************* MANY TO MANY ***************************************************************************************************

void WriteManyToManyActions(Controller controller)
{
	var manyToManyNavigations = controller.Entity.PresentationHierarchicalNavigationProperties
												 .Where(np => np.IsNavigable && np.IsManyToMany);
	
	if(!manyToManyNavigations.Any()) return;
#>

        #region Many-to-Many
<#+
	foreach(var nav in manyToManyNavigations)
	{
#>

        #region <#= nav.RoleName #>
<#+
		if(controller.Actions.Any(a => a.Type == ControllerActionType.DetailAdd && a.PropertyName != null && a.PropertyName.Equals(nav.RoleName)))
		{
#>

        /// <summary>
        /// Asocia uno o más objetos: <#= nav.Entity.Name #> a esta entidad
        /// </summary>
        /// <param name="parameters">Lista de referencias a asociar como parte de esta relación</param>
        /// <returns>OK, si la operación se completó correctamente</returns>
        [HttpPost]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.EditOperationName) #>
        public virtual IHttpActionResult Add<#= nav.RoleName #>(MultiSelectActionParameters parameters)
        {
            var result = Services<#= controller.Entity.AggregateName #>.Add<#= nav.RoleName #>(parameters.Id.Value, parameters.SimpleItems.Select(i => int.Parse(i.Id)));

            if (!result.HasErrors)
            {
                return Ok();
            }
            return ResultWithMessages(result);
        }
<#+
		}
		
		if(controller.Actions.Any(a => a.Type == ControllerActionType.DetailRemove && a.PropertyName != null && a.PropertyName.Equals(nav.RoleName)))
		{
#>
		
        /// <summary>
        /// Des-asocia uno o más objetos: <#= nav.Entity.Name #> a esta entidad
        /// </summary>
        /// <param name="parameters">Lista de referencias a desasociar</param>
        /// <returns>OK, si la operación se completó correctamente</returns>		
        [HttpPost]
		<#= WriteAuthorizeAttribute(controller.Entity.SecurityModule, SecurityOperationSettings.EditOperationName) #>
        public virtual IHttpActionResult Remove<#= nav.RoleName #>(MultiSelectActionParameters parameters)
        {
            var result = Services<#= controller.Entity.AggregateName #>.Remove<#= nav.RoleName #>(parameters.Id.Value, parameters.SimpleItems.Select(i => int.Parse(i.Id)));

            if (!result.HasErrors)
            {
                return Ok();
            }
            return ResultWithMessages(result);
        }
<#+
		}
#>
        #endregion <#= nav.RoleName #>
		
<#+
	}
#>

        #endregion Many-to-Many
<#+
}


// ******************************************************************************* AGGREGATE SUB-ELEMENTS *****************************************************************************************

void WriteAggregateSubElementsActions(Controller controller)
{
	var aggregate = controller.Entity as Aggregate;
	
	if(aggregate == null) return;
	
#>

        #region Aggregate Sub-Elements
		
<#+
	var processedEntities = new List<string>();
	
	foreach(var part in aggregate.NavigationParts.Where(np => !(np.Entity is ValueObject) && !np.IsOneToOne))
	{
		var navigationName = aggregate.NavigationParts.Count(np => np.Entity.Name.Equals(part.Entity.Name)) == 1 ?
							 part.Entity.Name : part.RoleName;
		
		var pluralizedNavigationName = aggregate.NavigationParts.Count(np => np.Entity.Name.Equals(part.Entity.Name)) == 1 ?
							 Pluralize(part.Entity.Name) : part.RoleName;
		var orderColumn = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedColumnOrders[part.Entity];
		var dto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[part.Entity];
							 
#>
        #region <#= navigationName #>
		
<#+
	if(controller.Actions.Any(a => a.Type == ControllerActionType.SubElementGet && a.PropertyName != null && a.PropertyName.Equals(part.RoleName)))
	{
#>
        /// <summary>
        /// Devuelve el contenido de un objeto: <#= part.Entity.Name #>
        /// </summary>
	    /// <param name="id<#= controller.Entity.Name #>">Identificador de la entidad raíz</param>
	    /// <param name="id<#= part.Entity.Name #>">Identificador del sub-elemento de agregado</param>
        /// <returns>El objeto: <#= part.Entity.Name #> o un error en caso de no existir</returns>
        [HttpGet]
		[Route("{id<#= controller.Entity.Name #>}/<#= Pluralize(Hyphenate(part.Entity.Name)) #>/{id<#= part.Entity.Name #>}", Name = "Get<#= part.Entity.Name #>")]
		<#= WriteAuthorizeAttribute(part.Entity.SecurityModule, SecurityOperationSettings.ShowOperationName) #>
        public virtual IHttpActionResult Get<#= navigationName #>(int id<#= controller.Entity.Name #>, int id<#= part.Entity.Name #>)
        {
            var result = Services<#= controller.Entity.AggregateName #>.Get<#= navigationName #>(id<#= part.Entity.Name #>);

            if (result.Type == ResultType.Ok && result.Element != null)
            {
                return Ok(result.Element);
            }
            return ResultWithMessages(result);
        }
<#+
	}

	if(controller.Actions.Any(a => a.Type == ControllerActionType.SubElementSearch && a.PropertyName != null && a.PropertyName.Equals(part.RoleName)))
	{
#>

        /// <summary>
        /// Devuelve los resultados de una "Búsqueda Avanzada" la cual consta de Filtrado, Proyección y Ordenacion Multiple
        /// </summary>
	    /// <param name="id<#= controller.Entity.Name #>">Identificador del objeto principal: <#= controller.Entity.Name #></param>		
        /// <param name="parameters">Parámetros de la búsqueda</param>
        /// <returns>Los resultados.</returns>
        [HttpPost]
		[Route("{id<#= controller.Entity.Name #>}/<#= Pluralize(Hyphenate(part.Entity.Name)) #>/advanced-search")]
		<#= WriteAuthorizeAttribute(part.Entity.SecurityModule, SecurityOperationSettings.ListOperationName) #>
        public virtual IHttpActionResult AdvancedSearch<#= pluralizedNavigationName #>(int id<#= controller.Entity.Name #>, <#= part.Entity.Name #>ListParameters parameters)
        {
            var pagination = parameters.GetPaginationInfo(<#= orderColumn.Name #>.<#= GetOrderingDefaultValue(part.Entity) #>);

            var specification = new <#= Pluralize(part.Entity.Name) #>ListSpecification
            {
                Id<#= part.Inverse.RoleName #> = parameters.IdAggregateRoot,
<#+
	WriteScalarsFilterSearch(part.Entity.HierarchicalScalars);
	
	foreach(var nav in part.Entity.HierarchicalSingleNavigationProperties.Where(np => np.IsNavigable && !np.IsPartOfAggregate))
	{
		if(nav.Entity is ValueObject)
		{
			WriteScalarsFilterSearch(nav.Entity.Scalars, nav.RoleName);
		}
		else
		{
#>
				FilterId<#= nav.RoleName #> = parameters.Filter<#= nav.RoleName #> != null ? parameters.Filter<#= nav.RoleName #>.Id : null,
<#+	
		}
	}
#>
                Pagination = pagination
            };
            var result = Services<#= controller.Entity.AggregateName #>.GetPaged<#= pluralizedNavigationName #>(specification);
            if (!result.HasErrors)
            {
                return OkPagedList(result);
            }
            return ResultWithMessages(result);
        }
<#+
	}
	
	if(controller.Actions.Any(a => a.Type == ControllerActionType.SubElementAdd && a.PropertyName != null && a.PropertyName.Equals(part.RoleName)))
	{
#>

        /// <summary>
        /// Añade un nuevo recurso/objeto: <#= part.Entity.Name #>.
        /// </summary>
        /// <param name="id<#= controller.Entity.Name #>">Identificador del objeto principal: <#= controller.Entity.Name #></param>		
        /// <param name="parameters">Los valores de la nueva instancia a almacenar</param>
        /// <returns>La información del objeto creado. Error y Mensajes si dicha operación no se pudo completar</returns>
        [HttpPost]
		[Route("{id<#= controller.Entity.Name #>}/<#= Pluralize(Hyphenate(part.Entity.Name)) #>")]
		<#= WriteAuthorizeAttribute(part.Entity.SecurityModule, SecurityOperationSettings.CreateOperationName) #>
        public virtual IHttpActionResult Add<#= navigationName #>(int id<#= controller.Entity.Name #>, <#= part.Entity.Name #>SaveParameters parameters)
        {
<#+
WriteSave(part.Entity, controller.Entity, dto, "Add" + navigationName, false, null);
#>
        }
<#+
	}
	
	if(controller.Actions.Any(a => a.Type == ControllerActionType.SubElementEdit && a.PropertyName != null && a.PropertyName.Equals(part.RoleName)))
	{
#>

	    /// <summary>
	    /// Modifica un recurso/objeto: <#= part.Entity.Name #> existente.
	    /// </summary>
        /// <param name="id<#= controller.Entity.Name #>">Identificador del objeto principal: <#= controller.Entity.Name #></param>
        /// <param name="id<#= part.Entity.Name #>">Identificador del sub-elemento de agregado: <#= part.Entity.Name #></param>
        /// <param name="parameters">Los nuevos valores a establecer en la instancia existente</param>
        /// <returns>OK, si la operación se pudo completar con éxito. NotFound ó Conflict en caso de Error</returns>
        [HttpPut]
		[Route("{id<#= controller.Entity.Name #>}/<#= Pluralize(Hyphenate(part.Entity.Name)) #>/{id<#= part.Entity.Name #>}")]
		<#= WriteAuthorizeAttribute(part.Entity.SecurityModule, SecurityOperationSettings.EditOperationName) #>
        public virtual IHttpActionResult Modify<#= navigationName #>(int id<#= controller.Entity.Name #>, int id<#= part.Entity.Name #>, <#= part.Entity.Name #>SaveParameters parameters)
        {
<#+
WriteSave(part.Entity, controller.Entity, dto, "Modify" + navigationName, true, "id" + part.Entity.Name);
#>
        }
<#+
	}
	
	if(controller.Actions.Any(a => a.Type == ControllerActionType.SubElementRemove && a.PropertyName != null && a.PropertyName.Equals(part.RoleName)))
	{
#>

	    /// <summary>
	    /// Elimina uno o varios recursos/objetos: <#= part.Entity.Name #> existentes.
	    /// </summary>
	    /// <param name="id<#= controller.Entity.Name #>">Identificador del objeto entidad raíz: <#= controller.Entity.Name #></param>
	    /// <param name="parameters">Identificadores de los objetos que se desean eliminar del almacenamiento</param>
	    /// <returns>OK, si la operación se pudo completar con exito. NotFound o Conflict en caso de Error</returns>
        [HttpPost]
		[Route("{id<#= controller.Entity.Name #>}/<#= Pluralize(Hyphenate(part.Entity.Name)) #>/remove")]
		<#= WriteAuthorizeAttribute(part.Entity.SecurityModule, SecurityOperationSettings.DeleteOperationName) #>
        public virtual IHttpActionResult Remove<#= pluralizedNavigationName #>(int id<#= controller.Entity.Name #>, MultiSelectActionParameters parameters)
        {
            var result = Services<#= controller.Entity.AggregateName #>.Remove<#= pluralizedNavigationName #>(id<#= controller.Entity.Name #>, parameters.Ids.ToArray());

            if (result.Type == ResultType.Ok
				&& !result.HasErrors)
            {
                return Ok();
            }
            return ResultWithMessages(result);
        }

<#+
	}

	WriteFileManagementActions(Controller, part);
#>
		
        #endregion <#= navigationName #>
<#+
		processedEntities.Add(part.Entity.Name);
	}
#>
		
        #endregion Aggregate Sub-Elements
<#+
}

void WriteSave(Entity entity, Entity svcEntity, Dto dto, string appMethodName, bool isEditing, string idEditEntityName)
{
#>
            var <#= ToCamel(entity.Name) #> = new <#= dto.Name #>();

			// Copiar los valores				
<#+
	WriteSaveScalars(entity, entity);
	
	if (entity.Parent != null && entity.InheritanceStrategy == InheritanceStrategy.Simple)
	{
#>
            <#= ToCamel(entity.Name) #>.<#= entity.Parent.Name #> = new <#= entity.Parent.Name #>Dto();
<#+
		WriteSaveScalars(entity, entity.Parent, "." + entity.Parent.Name);
		
		if (entity.Parent.Parent != null && entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
		{
#>
            <#= ToCamel(entity.Name) #>.<#= entity.Parent.Name #>.<#= entity.Parent.Parent.Name #> = new <#= entity.Parent.Parent.Name #>Dto();
<#+
			WriteSaveScalars(entity, entity.Parent.Parent, "." + entity.Parent.Name + "." + entity.Parent.Parent.Name);
		}
	}

	WriteSaveRelations(entity, entity);
	
	if (entity.Parent != null && entity.InheritanceStrategy == InheritanceStrategy.Simple)
	{
		WriteSaveRelations(entity, entity.Parent, "." + entity.Parent.Name);
		
		if (entity.Parent.Parent != null && entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			WriteSaveRelations(entity, entity.Parent.Parent, "." + entity.Parent.Name + "." + entity.Parent.Parent.Name);
		}
	}
	
	WriteSaveFiles(entity, entity);
	
	if (entity.Parent != null && entity.InheritanceStrategy == InheritanceStrategy.Simple)
	{
		WriteSaveFiles(entity, entity.Parent, "." + entity.Parent.Name);
		
		if (entity.Parent.Parent != null && entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			WriteSaveFiles(entity, entity.Parent.Parent, "." + entity.Parent.Name + "." + entity.Parent.Parent.Name);
		}
	}
	
	if(isEditing || !entity.AutoGenerateIdentity)
	{
#>

			// Copiar el valor del Id de la Entidad
            <#= ToCamel(entity.Name) #>.Id = <#= isEditing ? idEditEntityName : "parameters.Id" #>;
<#+
	}
#>

            var result = Services<#= svcEntity.AggregateName #>.<#= appMethodName #>(<#= ToCamel(entity.Name) #>);

            if (result.HasErrors)
			{
				return ResultWithMessages(result);
			}

<#+
	if(!isEditing)
	{
		var idOrigin = entity.AutoGenerateIdentity ? ("result.Element.Id") : (ToCamel(entity.Name) + ".Id");
		var getRoute = !entity.IsAggregatePart 
			? "new { id = " + idOrigin + " }" 
			: "new { id" + Controller.Entity.Name + ", id" + entity.Name + " = " + idOrigin + " }";
#>
            if (result.Type == ResultType.ElementCreated
                && result.Element != null)
            {
                return Created(
                    Url.Link("Get<#= entity.Name #>", <#= getRoute #>),
                    new
                    {
                        Id = <#= idOrigin #>,
                        <#= ToCamel(entity.Name) #>.DisplayName
                    });
			}
			return Ok();			
<#+
	}
	else 
	{
#>
			return Ok(new
				{
					Id = <#= ToCamel(entity.Name) #>.Id,
					<#= ToCamel(entity.Name) #>.DisplayName
				});
<#+
	}
}

void WriteSaveScalars(Entity current, Entity entity, string prefix = "")
{
	foreach(var scalar in entity.HierarchicalScalars.Where(s => !s.IsFileType))
	{
		if("time".Equals(scalar.Type))
		{
#>
            <#= ToCamel(current.Name) #><#= prefix #>.<#= scalar.Name #> = parameters.<#= scalar.Name #>;
<#+	
		}
		else
		{
#>
            <#= ToCamel(current.Name) #><#= prefix #>.<#= scalar.Name #> = parameters.<#= scalar.Name #>;
<#+
		}
	}
}

void WriteSaveRelations(Entity current, Entity entity, string prefix = "")
{
	foreach(var nav in entity.HierarchicalSingleNavigationProperties.Where(np => np.IsNavigable))
	{
		var targetDto = DddModelRoot.UnirDddModel.ApplicationModelRoot.IndexedDtos[nav.Entity];
		
		if(nav.Entity is ValueObject)
		{
#>
            <#= ToCamel(current.Name) #><#= prefix #>.<#= nav.RoleName #> = new <#= targetDto.Name #>() 
			{ 
<#+
		foreach(var scalar in nav.Entity.Scalars)
		{
#>
				<#= scalar.Name #> = parameters.<#= nav.RoleName #><#= scalar.Name #>,
<#+
		}
#>
			};
<#+
		}		
		else if(nav.IsMandatory)
		{
			if(nav.IsPartOfAggregate)
			{
#>
            <#= ToCamel(current.Name) #><#= prefix #>.<#= nav.RoleName #> = new <#= targetDto.Name #>() { Id = parameters.IdAggregateRoot };
<#+
			}
			else
			{
#>
            <#= ToCamel(current.Name) #><#= prefix #>.<#= nav.RoleName #> = new <#= targetDto.Name #>() { Id = parameters.<#= nav.RoleName #>.Id };
<#+
			}
		}
		else
		{
#>
            <#= ToCamel(current.Name) #><#= prefix #>.<#= nav.RoleName #> = parameters.<#= nav.RoleName #>.Id.HasValue
                                        ? new <#= targetDto.Name #> { Id = parameters.<#= nav.RoleName #>.Id.Value }
                                        : null;
<#+
		}
	}
}

void WriteSaveFiles(Entity current, Entity entity, string prefix = "")
{
	if(entity.HierarchicalScalars.Any(s => s.IsFileType))
	{
		foreach(var scalar in entity.HierarchicalScalars.Where(s => s.IsFileType))
		{
			var serviceEntityName = entity.IsAggregatePart ? entity.TopAggregateRoot.Name : entity.Name;
#>
            if (parameters.Has<#= scalar.Name #> && Request.Headers.Contains("X-TempFileId-<#= prefix #><#= scalar.Name #>"))
			{
			    var tempId = Request.Headers.GetValues("X-TempFileId-<#= prefix #><#= scalar.Name #>").FirstOrDefault();
                Services<#= entity.AggregateName #>.BindTempFileName(<#= ToCamel(current.Name) #><#= prefix #>, new Guid(tempId), a => a.<#= scalar.Name #>);	
			}
<#+
		}
	}
}
#>